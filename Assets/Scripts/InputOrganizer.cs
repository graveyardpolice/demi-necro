﻿using UnityEngine;
using System.Collections;

public class InputOrganizer : MonoBehaviour {

	string X, A, B, Y, rBumper, lBumper, rTrigger, lTrigger, start, rStickVertPos, rStickVertNeg, rStickHorNeg, rStickHorPos;

	// Use this for initialization
	void Start () {

#region OS Input Definitions
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX
		X = Keys.JoystickButton2;
		A = Keys.JoystickButton0;
		B = Keys.JoystickButton1;
		Y = Keys.JoystickButton3;
		start = Keys.JoystickButton7;
		rBumper = Keys.JoystickButton5;
		lBumper = Keys.JoystickButton4;
		rStickHorNeg = Keys.Joy1Axis4Negative;
		rStickHorPos = Keys.Joy1Axis4Positive;
		rStickVertNeg = Keys.Joy1Axis5Positive;
		rStickVertPos = Keys.Joy1Axis5Negative;

#elif UNITY_STANDALONE_OSX
		X = Keys.JoystickButton18;
		A = Keys.JoystickButton16;
		B = Keys.JoystickButton17;
		Y = Keys.JoystickButton19;
		start = Keys.JoystickButton9;
		rBumper = Keys.JoystickButton14;
		lBumper = Keys.JoystickButton13;
		rStickHorNeg = Keys.Joy1Axis3Negative;
		rStickHorPos = Keys.Joy1Axis3Positive;
		rStickVertNeg = Keys.Joy1Axis4Positive;
		rStickVertPos = Keys.Joy1Axis4Negative;

#endif
#endregion

#region Input Definitions (Keys)

		//Define keys
		cInput.SetKey("ShiftWorld", Keys.Space, A);
		cInput.SetKey("AltFire", Keys.LeftShift, B);
		cInput.SetKey("Interact", Keys.R, X);
		cInput.SetKey("UseItem", Keys.F, Y);
		//Weapon Change Inputs
		cInput.SetKey("ChangeWeapRight", Keys.E, rBumper);
		cInput.SetKey("ChangeWeapLeft", Keys.Q, lBumper);
		//Menu Input
		cInput.SetKey("Start", Keys.Escape, start);
		//Movement Inputs
		cInput.SetKey("LeftMove", Keys.A, Keys.Joy1Axis1Negative);
		cInput.SetKey("RightMove", Keys.D, Keys.Joy1Axis1Positive);
		cInput.SetKey("UpMove", Keys.W, Keys.Joy1Axis2Negative);
		cInput.SetKey("DownMove", Keys.S, Keys.Joy1Axis2Positive);
		//Shooting inputs
		cInput.SetKey("LeftShoot", Keys.ArrowLeft, rStickHorNeg);
		cInput.SetKey("RightShoot", Keys.ArrowRight, rStickHorPos);
		cInput.SetKey("UpShoot", Keys.ArrowUp, rStickVertPos);
		cInput.SetKey("DownShoot", Keys.ArrowDown, rStickVertNeg);


		//Define Axis
		//Movement Axis
		cInput.SetAxis("HorMove", "LeftMove", "RightMove");
		cInput.SetAxis("VertMove", "DownMove", "UpMove");
		//Shooting Axis
		cInput.SetAxis("HorShoot", "LeftShoot", "RightShoot");
		cInput.SetAxis("VertShoot", "DownShoot", "UpShoot");

#endregion
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
