﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour {
	Vector3 originalPosition;
	Transform portal;
	float shakeAmount;
	bool shakeVert;
	public bool isShaking;
	public int health;
	private Animator _anim;

	void Awake()
	{
		_anim = GetComponent<Animator>();
		portal = GetComponent<Transform>();
	}
	// Use this for initialization
	void Start () {
		originalPosition = portal.position;
	}

	void Update()
	{
		if(health <= 0)
		{
			EnemySpawn.portalCount--;
			gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Shakes the camera.
	/// </summary>
	/// <param name="strength">Strength</param>
	public void ShakeCamera(float strength, bool shakeVertical)
	{
		if(!isShaking)
		{
			isShaking = true;
			shakeAmount = strength;
			shakeVert = shakeVertical;
			InvokeRepeating("StartShake", 0f, .01f);
			Invoke("StopShake", .3f);
		}
	}
	
	
	private void StartShake()
	{
		if(shakeAmount > 0)
		{
			float quakeAmount = Random.value * shakeAmount * 2 - shakeAmount;
			Vector3 cPos = portal.position;
			cPos.y += shakeVert ? quakeAmount : 0;
			cPos.x += shakeVert ? 0 : quakeAmount;
			portal.position = cPos;
		}
	}
	
	private void StopShake()
	{
		if(isShaking)isShaking = false;
		CancelInvoke("StartShake");
		portal.position = originalPosition;
	}

	void OnTriggerEnter2D(Collider2D col)
	{

		   
	}
}
