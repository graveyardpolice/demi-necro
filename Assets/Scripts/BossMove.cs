﻿using UnityEngine;
using System.Collections;

public class BossMove : MonoBehaviour {
	private struct RaycastOrigins
	{
		public Vector3 topRight;
		public Vector3 topLeft;
		public Vector3 bottomRight;
		public Vector3 bottomLeft;
	}
	
	public int totalVerticalRays, totalHorizontalRays;
	private float _verticalDistanceBetweenRays, _horizontalDistanceBetweenRays;
	public float moveSpeed;
	public int health, startingHealth;
	public float normHorSpeed;
	public float normVertSpeed;
	public float damping;
	public LayerMask playerMask;
	private CharacterController2D _controller;
	private Transform _playerTrans, _enemyTrans;
	private Vector2 _velocity;
	private RaycastOrigins _raycastOrigins;
	private BoxCollider2D _box;
	private float _skinWidth;
	private RaycastHit2D _raycastHit;
	private Animator _anim;
	public float _timer;
	public GameObject deathSprite;
	public GameObject[] deathPool;
	private int deathNum;
	private bool spawnDeath;
	private SpriteRenderer _sprite;
	public AudioClip death, roar, hit;
	private AudioSource _audio;
	private bool audioplayed;
	public AudioClip[] playerhit = new AudioClip[3];
	
	// Use this for initialization
	void Awake () 
	{
		_audio = GetComponent<AudioSource>();
		_sprite = GetComponent<SpriteRenderer>();
		_anim = GetComponent<Animator>();
		_playerTrans = GameObject.Find("player").GetComponent<Transform>();
		_controller = GetComponent<CharacterController2D>();
		_skinWidth = _controller.skinWidth;
		_box = GetComponent<BoxCollider2D>();
		_raycastOrigins = new RaycastOrigins();
		_enemyTrans = GetComponent<Transform>();
	}
	
	void Start()
	{
		audioplayed = false;
		_box.enabled = true;
		health = startingHealth;
		recalculateDistanceBetweenRays();
		_timer = 0;
		_anim.SetInteger("Health", startingHealth);
		_audio.PlayOneShot(roar);
	}

	// Update is called once per frame
	void Update () 
	{
		if(health <= 0)
		{
			if(!audioplayed)_audio.PlayOneShot(death);
			audioplayed = true;
			EnemySpawn.lastPortal = false;
			EnemySpawn.bossDead = true;
			_box.enabled = false;
			_velocity.x = _velocity.y = 0;
			_anim.SetInteger("Health", health);
			if(_timer >= .33f)
			{
				if(!spawnDeath)EnemySpawn.SpawnSlimeDeath(_enemyTrans);
				spawnDeath = true;
				EventManager.WorldShiftInEvent -= Shift;
				EventManager.WorldShiftOutEvent -= UnShift;
				gameObject.SetActive(false);
			}
			else
				_timer += Time.deltaTime;
		}
		
		_velocity = _controller.velocity;

		if(_playerTrans.position.y > _enemyTrans.position.y)
		{
			_anim.SetBool("Up", true);
			_anim.SetBool("Down", false);
		}
		else if(_playerTrans.position.y < _enemyTrans.position.y)
		{
			_anim.SetBool("Up", false);
			_anim.SetBool("Down", true);
		}

		if(!CheckSides())
		{
			if(!CheckVertical())
			{
				normHorSpeed = _enemyTrans.position.x > _playerTrans.position.x ? -1 : 1;
				normVertSpeed = _enemyTrans.position.y > _playerTrans.position.y ? -1 : 1;
			}
			else
			{
				normHorSpeed = _enemyTrans.position.x > _playerTrans.position.x ? -1 : 1;
				normVertSpeed = 0;
			}
		}
		else
		{
			if(!CheckVertical())
			{
				normHorSpeed = 0;
				normVertSpeed = _enemyTrans.position.y > _playerTrans.position.y ? -1 : 1;
			}
			else if(CheckVertical(true))
			{
				normVertSpeed = -1;
				normHorSpeed = 0;
			}
			else if(CheckVertical(false))
			{
				normVertSpeed = 1;
				normHorSpeed = 0;
			}
			else
			{
				normHorSpeed = normVertSpeed = 0;
			}
		}
		
		if(health > 0) _velocity.x = Mathf.Lerp(_velocity.x, normHorSpeed * moveSpeed, Time.deltaTime * damping);
		if(health > 0)_velocity.y = Mathf.Lerp(_velocity.y, normVertSpeed * moveSpeed, Time.deltaTime * damping);
		
		if(health > 0)_controller.move(_velocity * Time.deltaTime);
	}
	
	public void Shift()
	{
		Color a = _sprite.color;
		a.a = .5f;
		_sprite.color = a;
	}
	public void UnShift()
	{
		Color a = _sprite.color;
		a.a = 1;
		_sprite.color = a;
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.layer == playerMask)
			normHorSpeed = normVertSpeed = 0;
		if(col.gameObject.name == "player")
		{
			Playercontrol2D pc = col.GetComponent<Playercontrol2D>();
			if(!pc.shift)
			{
				pc.health--;
				_audio.PlayOneShot(playerhit[Random.Range(0, playerhit.Length)]);

			}
			}
		if(col.gameObject.tag == "Bullet")
		{
			_audio.PlayOneShot(hit);
		}
	}
	
	void OnTriggerStay2D(Collider2D col)
	{
		if(col.gameObject.layer == playerMask)
			normHorSpeed = normVertSpeed = 0;
		if(col.gameObject.layer == LayerMask.NameToLayer("Portal"))
			normVertSpeed = -1;
	}
	
	bool CheckSides()
	{
		setRaycastOrigins();
		var isGoingRight = _enemyTrans.position.x < _playerTrans.position.x;
		var rayDirection = isGoingRight ? Vector2.right : -Vector2.right;
		var initialRayOrigin = isGoingRight ? _raycastOrigins.bottomRight : _raycastOrigins.bottomLeft;
		var rayDistance = 3f;
		for(int i = 0; i < totalHorizontalRays; i++)
		{
			var ray = new Vector2(initialRayOrigin.x, initialRayOrigin.y + i * _horizontalDistanceBetweenRays);
			_raycastHit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
			if(_raycastHit)
				return true;
		}
		return false;
	}
	
	bool CheckSides(bool right)
	{
		setRaycastOrigins();
		var rayDirection = right ? Vector2.right : -Vector2.right;
		var initialRayOrigin = right ? _raycastOrigins.bottomRight : _raycastOrigins.bottomLeft;
		var rayDistance = 3f;
		for(int i = 0; i < totalHorizontalRays; i++)
		{
			var ray = new Vector2(initialRayOrigin.x, initialRayOrigin.y + i * _horizontalDistanceBetweenRays);
			_raycastHit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
			if(_raycastHit)
				return true;
		}
		return false;
	}
	
	/// <summary>
	/// Checks the vertical.
	/// </summary>
	/// <returns><c>true</c>, if we have an object in the direction we are travelling, <c>false</c> otherwise.</returns>
	bool CheckVertical()
	{
		setRaycastOrigins();
		var isGoingUp = _enemyTrans.position.y < _playerTrans.position.y;
		var rayDirection = isGoingUp ? Vector2.up: -Vector2.up;
		var initialRayOrigin = isGoingUp ? _raycastOrigins.topLeft : _raycastOrigins.bottomLeft;
		var rayDistance = 3f;
		for(int i = 0; i < totalVerticalRays; i++)
		{
			var ray = new Vector2(initialRayOrigin.x + i * _verticalDistanceBetweenRays, initialRayOrigin.y);
			_raycastHit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
			if(_raycastHit)
				return true;
		}
		return false;
	}
	
	bool CheckVertical(bool up)
	{
		setRaycastOrigins();
		var rayDirection = up ? Vector2.up: -Vector2.up;
		var initialRayOrigin = up ? _raycastOrigins.topLeft : _raycastOrigins.bottomLeft;
		var rayDistance = 3f;
		for(int i = 0; i < totalVerticalRays; i++)
		{
			var ray = new Vector2(initialRayOrigin.x + i * _verticalDistanceBetweenRays, initialRayOrigin.y);
			_raycastHit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
			if(_raycastHit)
				return true;
		}
		return false;
	}
	
	
	/// <summary>
	/// Recalculates the distance between rays if we change the number of rays in code we must call this.
	/// </summary>
	public void recalculateDistanceBetweenRays()
	{
		// figure out the distance between our rays in both directions
		// horizontal
		var colliderUseableHeight = _box.size.y * Mathf.Abs( transform.localScale.y ) - ( 2f * _skinWidth );
		_verticalDistanceBetweenRays = colliderUseableHeight / ( totalHorizontalRays - 1 );
		
		// vertical
		var colliderUseableWidth = _box.size.x * Mathf.Abs( transform.localScale.x ) - ( 2f * _skinWidth );
		_horizontalDistanceBetweenRays = colliderUseableWidth / ( totalVerticalRays - 1 );
	}
	
	
	/// <summary>
	/// Sets the raycast origins, must be called anytime you cast rays.
	/// </summary>
	private void setRaycastOrigins()
	{
		var	scaledColliderSize = new Vector2 (_box.size.x * Mathf.Abs (transform.localScale.x), _box.size.y * Mathf.Abs (transform.localScale.y)) / 2;
		var scaledCenter = new Vector2( _box.center.x * transform.localScale.x, _box.center.y * transform.localScale.y );
		
		_raycastOrigins.topRight = transform.position + new Vector3( scaledCenter.x + scaledColliderSize.x, scaledCenter.y + scaledColliderSize.y );
		_raycastOrigins.topRight.x += _skinWidth;
		_raycastOrigins.topRight.y += _skinWidth;
		
		_raycastOrigins.topLeft = transform.position + new Vector3( scaledCenter.x - scaledColliderSize.x, scaledCenter.y + scaledColliderSize.y );
		_raycastOrigins.topLeft.x -= _skinWidth;
		_raycastOrigins.topLeft.y += _skinWidth;
		
		_raycastOrigins.bottomRight = transform.position + new Vector3( scaledCenter.x + scaledColliderSize.x, scaledCenter.y -scaledColliderSize.y );
		_raycastOrigins.bottomRight.x += _skinWidth;
		_raycastOrigins.bottomRight.y -= _skinWidth;
		
		_raycastOrigins.bottomLeft = transform.position + new Vector3( scaledCenter.x - scaledColliderSize.x, scaledCenter.y -scaledColliderSize.y );
		_raycastOrigins.bottomLeft.x -= _skinWidth;
		_raycastOrigins.bottomLeft.y -= _skinWidth;
	}
}
