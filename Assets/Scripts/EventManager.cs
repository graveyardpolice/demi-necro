﻿using UnityEngine;
using System.Collections;
using System;

public class EventManager : MonoBehaviour{

#region Events and Delegates
	public static event Action WorldShiftInEvent;
	public static event Action WorldShiftOutEvent;
#endregion

	public static void Shift()
	{
		if(WorldShiftInEvent != null)
			WorldShiftInEvent();
	}

	public static void UnShift()
	{
		if(WorldShiftOutEvent != null)
			WorldShiftOutEvent();
	}

}
