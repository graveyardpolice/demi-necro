﻿using UnityEngine;
using System.Collections;

public class BaseGun : MonoBehaviour {

	public GameObject bullet;
	public GameObject flash;
	public float shotDelay;
	private Transform _gunBarrel;
	private GameObject[] bulletPool, flashPool;
	private int bulletNum;
	private int flashNum;
	private float _timer;
	public int ammo;
	private AudioSource[] gunShots = new AudioSource[2];
	public AudioClip fire;

	// Use this for initialization
	void Start () 
	{
		gunShots[0] = GameObject.FindGameObjectWithTag("GunBody").GetComponent<AudioSource>();
		gunShots[1] = GameObject.FindGameObjectWithTag("GunBarrel").GetComponent<AudioSource>();
		bulletPool = new GameObject[100];
		flashPool = new GameObject[10];
		for(int i = 0; i < bulletPool.Length; i++)
		{
			bulletPool[i] = (GameObject)Instantiate(bullet);
			bulletPool[i].hideFlags = HideFlags.HideInHierarchy;
			bulletPool[i].SetActive(false);
		}
		for(int i = 0; i < flashPool.Length; i++)
		{
			flashPool[i] = (GameObject)Instantiate(flash);
			flashPool[i].hideFlags = HideFlags.HideInHierarchy;
			flashPool[i].SetActive(false);
		}
		_timer = 0;
		_gunBarrel = GameObject.Find("GunBarrel").GetComponent<Transform>();
	}

	void Update()
	{
		_timer += Time.deltaTime;
	}

	public void MainShoot(float Angle, int dir)
	{
		if(_timer >= shotDelay && ammo > 0)
		{
			if(flashNum > 0) 
			{
				flashPool[flashPool.Length -1].SetActive(false);
				flashPool[flashNum - 1].SetActive(false);
			}
			GameObject flash = flashPool[flashNum];
			Light flashLight = flash.GetComponentInChildren<Light>();
			Animator flashAnim = flash.GetComponent<Animator>();
			GameObject go = bulletPool[bulletNum];
			BulletMove gobm = go.GetComponent<BulletMove>();
			go.SetActive(true);
			flash.SetActive(true);
			flashAnim.Play("MuzzleFlash");
			gobm.dir = dir;
			AudioSource source = flashNum % 2 == 0 ? gunShots[1] : gunShots[0];
			source.PlayOneShot(fire);
			flash.transform.position = _gunBarrel.position;
			go.transform.position = _gunBarrel.position;
			go.transform.rotation = Quaternion.Euler(0,0,Angle);
			if(bulletNum == bulletPool.Length - 1)
				bulletNum = 0;
			else
				bulletNum++;
			if(flashNum == flashPool.Length -1)
				flashNum = 0;
			else
				flashNum++;
			_timer = 0;
			ammo--;
		}
	}

	void AltShoot(){}
}
