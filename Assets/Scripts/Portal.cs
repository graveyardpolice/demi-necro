﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour {

	public float spawnDelay;
	public float difIncrement;
	public int maxStartSpawn;
	public int difficulty;
	public float distanceToSpawn;
	private float _timer;
	private float difcache;
	private Transform portal;
	private Transform _player;
	private float difTimer;


	void Awake()
	{
		_player = GameObject.Find("player").GetComponent<Transform>();
		portal = GetComponent<Transform>();
	}
	// Use this for initialization
	void Start () 
	{
		difcache = difIncrement;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Vector3.Distance(portal.position, _player.position) < distanceToSpawn)_timer += Time.deltaTime;
		difTimer += Time.deltaTime;
		if(difTimer > difIncrement)
		{
			difficulty++;
			Debug.Log("Difficulty increased to: " + difficulty);
			maxStartSpawn += difficulty % 2 == 0 ? 1 : 0;
			difIncrement += difcache;
		}
		if(difficulty > 10)
			difficulty = 10;
		if(_timer >= spawnDelay)
		{
			_timer = 0;
			if(difficulty <= 2)
				EnemySpawn.SpawnBaseEnemy(Random.Range(Mathf.FloorToInt(maxStartSpawn / 2), maxStartSpawn + 1), portal);
			else if(difficulty > 2 && difficulty < 10)
			{
				var num2 = Random.Range(Mathf.FloorToInt(maxStartSpawn / 2), maxStartSpawn + 1);
				var num = Random.Range(1,num2);
				EnemySpawn.SpawnBaseEnemy(num2, portal);
				EnemySpawn.SpawnSecondEnemy(num, portal);
			}
			else
				EnemySpawn.SpawnSecondEnemy(Random.Range(Mathf.FloorToInt(maxStartSpawn / 2), maxStartSpawn + 1), portal);
		}

	}
}
