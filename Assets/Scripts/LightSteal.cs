﻿using UnityEngine;
using System.Collections;

public class LightSteal : MonoBehaviour {

	Transform _trans;
	public bool lerp;
	public Transform _player;
	public float speed;

	// Use this for initialization
	void Awake () {
		_trans = GetComponent<Transform>();
		_player = GameObject.Find("player").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if(lerp)
		{
			Vector3 pos = _trans.position;
			pos.x = Mathf.Lerp(pos.x, _player.position.x, Time.deltaTime * speed);
			pos.y = Mathf.Lerp(pos.x, _player.position.x, Time.deltaTime * speed);
			_trans.position = pos;
		}
	}
}
