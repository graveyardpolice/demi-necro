﻿using UnityEngine;
using System.Collections;

public class GameScreen : MonoBehaviour {
	public static int deaths;
	public static bool dead;
	private GameObject player;
	private Playercontrol2D _playerControl;
	public float fadeSpeed;
	private Color col;
	public bool fadeIn;
	private BaseGun _gun;
	private string str;
	private Vector3 playscale;
	private Quaternion playrot;
	public enum Menu
	{
		First,
		Second,
		Third,
		Fourth,
		Fifth,
		Sixth,
		Seventh,
		Eight,
		Nine
	}

	public Menu _state;
	public GUISkin skin;
	void Awake() 
	{
		player = GameObject.Find("player");
		_gun = GameObject.Find("player").GetComponent<BaseGun>();
		_playerControl = GameObject.Find ("player").GetComponent<Playercontrol2D>();
	}

	// Use this for initialization
	void Start () 
	{
		playscale = player.transform.localScale;
		playrot = GameObject.Find("GunAnchor").transform.rotation;
		fadeIn = false;
		_state = Menu.First;
		col = Color.white;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(dead)
			_state = Menu.Nine;
		if(dead && cInput.GetKeyDown("Start"))
		{
			player.SetActive(true);
			Camera.main.transform.parent = player.transform;
			Camera.main.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
			player.transform.position = _playerControl.spawnPos;
			_playerControl.health = 4;
			player.transform.localScale = playscale;
			Transform go = GameObject.Find("GunAnchor").transform;
			go.localRotation = playrot;
			_gun.ammo = 0;
			_state = Menu.Seventh;
			dead = false;
			col = Color.white;
			_playerControl.facedDir = Playercontrol2D.PlayerFacing.Down;
		}
		if(EnemySpawn.portalCount == 0)
			_state = Menu.Eight;
		if(_playerControl.canShift)
			str = "Ready!";
		else
			str = "Recharging";
		if(fadeIn && col.a < 1)
			col.a += Time.deltaTime * fadeSpeed;
		else
		{
			fadeIn = false;
			if(_state != Menu.Seventh && _state != Menu.Eight && _state != Menu.Nine)col.a -= Time.deltaTime * fadeSpeed;
			if(col.a <=0 && _state != Menu.Seventh && _state != Menu.Eight && _state != Menu.Nine) NextState();
		}
	}

	void OnGUI()
	{
		GUI.skin = skin;
		GUI.color = col;
		switch(_state)
		{
		case Menu.First:
			GUI.Label(new Rect(0 ,Screen.height - 25, Screen.width, 100), "Created by: GraveyardPolice - @GravyardPolice on Twitter");
			break;
		case Menu.Second:
			GUI.Label(new Rect(0 ,Screen.height - 25, Screen.width, 100), "Programming by: Dustin Sims");
			break;
		case Menu.Third:
			GUI.Label(new Rect(0 ,Screen.height - 25, Screen.width, 100), "Art/Sound by: Britt Brady");
			break;
		case Menu.Fourth:
			GUI.Label(new Rect(0 ,Screen.height - 25, Screen.width, 100), "Press A (XBOX360) / Space to find the lost souls");
			break;
		case Menu.Fifth:
			GUI.Label(new Rect(0 ,Screen.height - 25, Screen.width, 100), "WASD / Left Stick to move, Arrow Keys / Right Stick to Shoot");
			break;
		case Menu.Sixth:
			GUI.Label(new Rect(0 ,Screen.height - 25, Screen.width, 100), "Find souls to power your gun");
			break;
		case Menu.Seventh:
			GUI.Label(new Rect(0 ,Screen.height - 25, Screen.width, 100),
			          "Health: " + _playerControl.health.ToString() + " | Souls: " + _gun.ammo.ToString() + 
			          " | Soul Shift: " + str + " | Portals Left: " + EnemySpawn.portalCount.ToString());
			break;
		case Menu.Eight:
			GUI.Label(new Rect(0 ,Screen.height/2 - 25, Screen.width, 100), "You saved the world in: " + GameScreen.deaths.ToString() + " Deaths");
			break;
		case Menu.Nine:
			GUI.Label(new Rect(0 ,Screen.height/2 - 25, Screen.width, 100), "You have died, press Start (XBOX360) / Escape to respawn");
			break;
		}
	}

	private void NextState()
	{
		switch(_state)
		{
		case Menu.First:
			fadeIn = true;
			if(col.a <=0)_state = Menu.Second;
			break;
		case Menu.Second:
			fadeIn = true;
			if(col.a <=0)_state = Menu.Third;
			break;
		case Menu.Third:
			fadeIn = true;
			if(col.a <=0)_state = Menu.Fourth;
			break;
		case Menu.Fourth:
			fadeIn = true;
			if(col.a <=0)_state = Menu.Fifth;
			break;
		case Menu.Fifth:
			fadeIn = true;
			if(col.a <=0)_state = Menu.Sixth;
			break;
		case Menu.Sixth:
			fadeIn = true;
			if(col.a <=0)_state = Menu.Seventh;
			break;
		}
	}
}
