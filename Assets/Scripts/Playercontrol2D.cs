﻿using UnityEngine;
using System.Collections;

public class Playercontrol2D : MonoBehaviour {

#region Internal Types
	private struct RaycastOrigins
	{
		public Vector3 topRight;
		public Vector3 topLeft;
		public Vector3 bottomRight;
		public Vector3 bottomLeft;
	}

	public enum PlayerFacing 
	{
		Up = 1,
		Down = 2,
		Left = 3,
		Right = 4
	}
#endregion

#region Public Variables
	//Movement
	public float moveSpeed, damping, normHorSpeed, normVertSpeed, gunDamping;
	//Raycasting
	public int totalVerticalRays;
	public int totalHorizontalRays;
	public PlayerFacing facedDir;
	public float moveMult;
	public int health;
	public GameObject flash;
	public AudioClip footstep;
	public float shiftTime;
	public float shiftCd;
	private float shiftDelay;
	public Vector3 spawnPos;
#endregion

#region Private Variables
	//Local Cache
	private RaycastOrigins _raycastOrigins;
	private BoxCollider2D _box;
	private CharacterController2D _controller;
	private SpriteRenderer _sprite;
	private Animator _anim;
	private Transform _trans;
	private GameObject[] flashPool;
	private int flashNum;

	//Local variables
	private float _skinWidth, _verticalDistanceBetweenRays, _horizontalDistanceBetweenRays;
	private Vector2 _velocity;
	private SpriteRenderer _gunSprite;
	private BaseGun _gun;
	public bool shift, canShift;
	private AudioSource _audio, shiftAudio, collectAudio;
	private float _timer;
	public bool dead;
	public AudioClip collect, shiftIn, shiftOut;
#endregion

	// Use this for initialization
	void Awake () 
	{
		_trans = GetComponent<Transform>();
		shiftAudio = GameObject.FindGameObjectWithTag("ShiftSounds").GetComponent<AudioSource>();
		collectAudio = GameObject.FindGameObjectWithTag("SoulCollect").GetComponent<AudioSource>();
		_audio = GetComponent<AudioSource>();
		_gun = GetComponent<BaseGun>();
		facedDir = PlayerFacing.Down;
		_raycastOrigins = new RaycastOrigins();
		_box = GetComponent<BoxCollider2D>();
		_controller = GetComponent<CharacterController2D>();
		_sprite = GetComponent<SpriteRenderer>();
		_anim = GetComponent<Animator>();
		_gunSprite = GameObject.Find("GunBody").GetComponent<SpriteRenderer>();
		_skinWidth = _controller.skinWidth;
		recalculateDistanceBetweenRays();
	}

	void Start()
	{
		spawnPos = _trans.position;
		flashPool = new GameObject[20];
		for(int i = 0; i < flashPool.Length; i++)
		{
			flashPool[i] = (GameObject)Instantiate(flash);
			flashPool[i].hideFlags = HideFlags.HideInHierarchy;
			flashPool[i].SetActive(false);
		}
		_timer = 0;
		canShift = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(health <= 0)
		{
			GameScreen.deaths++;
			GameScreen.dead = true;
			Camera.main.transform.parent = null;
			gameObject.SetActive(false);
		}
		if(shift && _timer < shiftTime)
			_timer += Time.deltaTime;
		if(!canShift && !shift && shiftDelay < shiftCd)
		{
			shiftDelay += Time.deltaTime;
			if(shiftDelay >= shiftCd)
			{
				canShift = true;
				shiftDelay = 0;
				_timer = 0;
			}
		}
		if(cInput.GetKey("ShiftWorld") && canShift)
			EventManager.Shift();
		else if(cInput.GetKeyUp("ShiftWorld") || _timer >= shiftTime)
		{
			EventManager.UnShift();
			_timer = 0;
		}
			

		switch(facedDir)
		{
		case PlayerFacing.Down:
			_gunSprite.sortingOrder = 2;
			_anim.SetBool("DownWalk", true);
			_anim.SetBool("UpWalk", false);
			_anim.SetBool("RightWalk", false);
			_anim.SetBool("LeftWalk", false);
			if(transform.localScale.x != 1) transform.localScale = new Vector3( 1, 1, 1);
			break;
		case PlayerFacing.Left:
			_gunSprite.sortingOrder = 0;
			_anim.SetBool("DownWalk", false);
			_anim.SetBool("UpWalk", false);
			_anim.SetBool("RightWalk", false);
			_anim.SetBool("LeftWalk", true);
			if(transform.localScale.x != 1) transform.localScale = new Vector3( 1, 1, 1);
			break;
		case PlayerFacing.Right:
			_gunSprite.sortingOrder = 2;
			_anim.SetBool("DownWalk", false);
			_anim.SetBool("UpWalk", false);
			_anim.SetBool("RightWalk", true);
			_anim.SetBool("LeftWalk", false);
			if(transform.localScale.x != -1) transform.localScale = new Vector3( -1, 1, 1);
			break;
		case PlayerFacing.Up:
			_gunSprite.sortingOrder = 0;
			_anim.SetBool("DownWalk", false);
			_anim.SetBool("UpWalk", true);
			_anim.SetBool("RightWalk", false);
			_anim.SetBool("LeftWalk", false);
			if(transform.localScale.x != -1) transform.localScale = new Vector3( -1, 1, 1);
			break;
		}

		//Cache velocity from controller
		_velocity = _controller.velocity;

		//MOVEMENT INPUTS
		var adjMove = shift ? moveMult : 1;
		normHorSpeed = cInput.GetAxisRaw("HorMove") * adjMove;
		normVertSpeed = cInput.GetAxisRaw("VertMove") * adjMove;

		if(cInput.GetAxisRaw("HorShoot") == 1)
		{
			if(facedDir == PlayerFacing.Right && !shift) _gun.MainShoot(-90, 1);
			facedDir = PlayerFacing.Right;
		}
		if(cInput.GetAxisRaw("HorShoot") == -1)
		{
			if(facedDir == PlayerFacing.Left && !shift) _gun.MainShoot(90, 0);
			facedDir = PlayerFacing.Left;
		}
		if(cInput.GetAxisRaw("VertShoot") == 1)
		{
			if(facedDir == PlayerFacing.Up && !shift) _gun.MainShoot(0, 2);
			facedDir = PlayerFacing.Up;
		}
		if(cInput.GetAxisRaw("VertShoot") == -1)
		{
			if(facedDir == PlayerFacing.Down && !shift) _gun.MainShoot(180, 3);
			facedDir = PlayerFacing.Down;
		}
		if(cInput.GetAxisRaw("HorMove") == 1 && cInput.GetAxisRaw("VertShoot") == 0 && cInput.GetAxisRaw("HorShoot") == 0)
		{
			facedDir = PlayerFacing.Right;
		}
		if(cInput.GetAxisRaw("HorMove") == -1 && cInput.GetAxisRaw("VertShoot") == 0 && cInput.GetAxisRaw("HorShoot") == 0)
		{
			facedDir = PlayerFacing.Left;
		}
		if(cInput.GetAxisRaw("VertMove") == 1 && cInput.GetAxisRaw("VertShoot") == 0 && cInput.GetAxisRaw("HorShoot") == 0)
		{
			facedDir = PlayerFacing.Up;
		}
		if(cInput.GetAxisRaw("VertMove") == -1 && cInput.GetAxisRaw("VertShoot") == 0 && cInput.GetAxisRaw("HorShoot") == 0)
		{
			facedDir = PlayerFacing.Down;
		}


		var absSpeed = Mathf.Abs(normHorSpeed) + Mathf.Abs(normVertSpeed);
		_anim.SetFloat("Speed", absSpeed);
		//Apply movement
		_velocity.y = Mathf.Lerp(_velocity.y, normVertSpeed * moveSpeed, Time.deltaTime * damping);
		_velocity.x = Mathf.Lerp(_velocity.x, normHorSpeed * moveSpeed, Time.deltaTime * damping);

		_controller.move(_velocity * Time.deltaTime);
	
	}

	#region Enable Disable

	void OnEnable()
	{
		EventManager.WorldShiftInEvent += Shift;
		EventManager.WorldShiftOutEvent += UnShift;
	}

	void OnDisable()
	{
		EventManager.WorldShiftInEvent -= Shift;
		EventManager.WorldShiftOutEvent -= UnShift;
	}

	#endregion

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer("Enemy"))
		{
			if(shift)
			{
				EnemyBase ebase = col.GetComponent<EnemyBase>();
				ebase.health = 0;
				EnemySpawn.enemyCount--;
				if(!ebase.stolen)
				{
					collectAudio.PlayOneShot(collect);
					_gun.ammo += ebase.souls;
					ebase.stolen = true;
					if(flashNum > 0) 
					{
						flashPool[flashPool.Length -1].SetActive(false);
						flashPool[flashNum - 1].SetActive(false);
					}
					GameObject go = flashPool[flashNum];
					go.transform.position = col.transform.position;
					go.SetActive(true);
					if(flashNum >= flashPool.Length - 1)
						flashNum = 0;
					else
						flashNum++;
				}
			}
		}
		if(col.gameObject.layer == LayerMask.NameToLayer("Enemy2"))
		{
			if(shift)
			{
				Slime2 ebase = col.GetComponent<Slime2>();
				ebase.health = 0;
				EnemySpawn.enemyCount--;
				if(!ebase.stolen)
				{
					collectAudio.PlayOneShot(collect);
					_gun.ammo += ebase.souls;
					ebase.stolen = true;
					if(flashNum > 0) 
					{
						flashPool[flashPool.Length -1].SetActive(false);
						flashPool[flashNum - 1].SetActive(false);
					}
					GameObject go = flashPool[flashNum];
					go.transform.position = col.transform.position;
					go.SetActive(true);
					if(flashNum >= flashPool.Length - 1)
						flashNum = 0;
					else
						flashNum++;
				}
			}
		}

	}

	void Shift()
	{
		shiftAudio.PlayOneShot(shiftIn);
		Color a = _sprite.color;
		a.a = .5f;
		_sprite.color = a;
		_gunSprite.color = a;
		shift = true;
		canShift = false;
	}
	public void PlayFootstep()
	{
		_audio.PlayOneShot(footstep);
	}
	void UnShift()
	{
		if(shift)shiftAudio.PlayOneShot(shiftOut);
		Color a = _sprite.color;
		a.a = 1;
		_sprite.color = a;
		_gunSprite.color = a;
		shift = false;
	}
	/// <summary>
	/// Recalculates the distance between rays if we change the number of rays in code we must call this.
	/// </summary>
	public void recalculateDistanceBetweenRays()
	{
		// figure out the distance between our rays in both directions
		// horizontal
		var colliderUseableHeight = _box.size.y * Mathf.Abs( transform.localScale.y ) - ( 2f * _skinWidth );
		_verticalDistanceBetweenRays = colliderUseableHeight / ( totalHorizontalRays - 1 );
		
		// vertical
		var colliderUseableWidth = _box.size.x * Mathf.Abs( transform.localScale.x ) - ( 2f * _skinWidth );
		_horizontalDistanceBetweenRays = colliderUseableWidth / ( totalVerticalRays - 1 );
	}
	
	
	/// <summary>
	/// Sets the raycast origins, must be called anytime you cast rays.
	/// </summary>
	private void setRaycastOrigins()
	{
		var	scaledColliderSize = new Vector2 (_box.size.x * Mathf.Abs (transform.localScale.x), _box.size.y * Mathf.Abs (transform.localScale.y)) / 2;
		var scaledCenter = new Vector2( _box.center.x * transform.localScale.x, _box.center.y * transform.localScale.y );
		
		_raycastOrigins.topRight = transform.position + new Vector3( scaledCenter.x + scaledColliderSize.x, scaledCenter.y + scaledColliderSize.y );
		_raycastOrigins.topRight.x -= _skinWidth;
		_raycastOrigins.topRight.y -= _skinWidth;
		
		_raycastOrigins.topLeft = transform.position + new Vector3( scaledCenter.x - scaledColliderSize.x, scaledCenter.y + scaledColliderSize.y );
		_raycastOrigins.topLeft.x += _skinWidth;
		_raycastOrigins.topLeft.y -= _skinWidth;
		
		_raycastOrigins.bottomRight = transform.position + new Vector3( scaledCenter.x + scaledColliderSize.x, scaledCenter.y -scaledColliderSize.y );
		_raycastOrigins.bottomRight.x -= _skinWidth;
		_raycastOrigins.bottomRight.y += _skinWidth;
		
		_raycastOrigins.bottomLeft = transform.position + new Vector3( scaledCenter.x - scaledColliderSize.x, scaledCenter.y -scaledColliderSize.y );
		_raycastOrigins.bottomLeft.x += _skinWidth;
		_raycastOrigins.bottomLeft.y += _skinWidth;
	}
}
