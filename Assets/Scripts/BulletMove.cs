﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {

	public float moveSpeed;
	private Transform _trans;
	public int dir;
	public LayerMask hits;

	// Use this for initialization
	void Awake () {
		_trans = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 pos = _trans.position;
		if(dir == 0)
			pos.x += -moveSpeed * Time.deltaTime;
		else if(dir == 1)
			pos.x += moveSpeed * Time.deltaTime;
		else if(dir == 2)
			pos.y += moveSpeed * Time.deltaTime;
		else if(dir == 3)
			pos.y += -moveSpeed * Time.deltaTime;

		_trans.position = pos;
	}
	void OnBecameInvisible()
	{
		gameObject.SetActive(false);
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer("Enemy"))
		{
			EnemyBase enbase = col.GetComponent<EnemyBase>();
			enbase.health--;
			EnemySpawn.enemyCount--;
			gameObject.SetActive(false);
		}
		if(col.gameObject.layer == LayerMask.NameToLayer("Enemy2"))
		{
			Slime2 slime = col.GetComponent<Slime2>();
			slime.health--;
			if(slime.health <= 0)EnemySpawn.enemyCount--;
			gameObject.SetActive(false);
		}
		if(col.gameObject.layer == LayerMask.NameToLayer("Portal") && !EnemySpawn.lastPortal)
		{
			NewBehaviourScript nbs = col.GetComponent<NewBehaviourScript>();
			nbs.health--;
			nbs.ShakeCamera(2f, false);
			gameObject.SetActive(false);
		}
		if(col.gameObject.layer == LayerMask.NameToLayer("Boss"))
		{
			BossMove slime = col.GetComponent<BossMove>();
			slime.health--;
			gameObject.SetActive(false);
		}
	}
	
}
