﻿using UnityEngine;
using System.Collections;

public class LightSwap : MonoBehaviour {

	public float speed;
	private Transform _light;
	private Quaternion _original;
	private Quaternion _rotation;

	private bool rotate;

	// Use this for initialization
	void Start () 
	{
		_light = GetComponent<Transform>();
		_original = transform.rotation;
		_rotation = Quaternion.Euler(90,0,0);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(rotate)
			_light.rotation = Quaternion.Lerp(_light.rotation, _rotation, Time.deltaTime * 9);
		else
			_light.rotation = Quaternion.Lerp(_light.rotation, _original, Time.deltaTime);
	}
	
	void OnEnable()
	{
		EventManager.WorldShiftInEvent += Shift;
		EventManager.WorldShiftOutEvent += UnShift;
	}
	void OnDisable()
	{
		EventManager.WorldShiftInEvent -= Shift;
		EventManager.WorldShiftOutEvent -= UnShift;
	}

	private void Shift()
	{
		rotate = true;
	}
	private void UnShift()
	{
		rotate = false;
	}
}
