﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {

	public GameObject slime;
	public GameObject slimeLvl2;
	public GameObject slimeDeath;
	public static GameObject[] slimePool;
	public static GameObject[] slimeDeathPool;
	public static GameObject[] slimePoolLvl2;
	private static int slimeNum;
	private static int slimeSpawnNum;
	private static int slimeSpawnLvl2Num;
	public float spawnTime;
	public float difficulty;
	private float _timer;
	private Transform _player;
	private float _gameTime;
	public int difIncrement;
	public static int maxEnemies;
	public static int enemyCount;
	public static int portalCount;
	public GameObject boss;
	private bool spawnedBoss;
	public static bool lastPortal;
	public static bool bossDead;

	// Use this for initialization
	void Start () 
	{
		portalCount = 15;
		maxEnemies = 25;
		_player = GameObject.Find("player").GetComponent<Transform>();
		slimeNum = 0;
		slimeDeathPool = new GameObject[100];
		slimePool = new GameObject[400];
		slimePoolLvl2 = new GameObject[400];
		for(int i = 0; i < slimeDeathPool.Length; i++)
		{
			slimeDeathPool[i] = (GameObject)Instantiate(slimeDeath);
			slimeDeathPool[i].hideFlags = HideFlags.HideInHierarchy;
			slimeDeathPool[i].SetActive(false);
		}
		for(int i = 0; i < slimePool.Length; i++)
		{
			slimePool[i] = (GameObject)Instantiate(slime);
			slimePool[i].hideFlags = HideFlags.HideInHierarchy;
			slimePool[i].SetActive(false);
		}
		for(int i = 0; i < slimePoolLvl2.Length; i++)
		{
			slimePoolLvl2[i] = (GameObject)Instantiate(slimeLvl2);
			slimePoolLvl2[i].hideFlags = HideFlags.HideInHierarchy;
			slimePoolLvl2[i].SetActive(false);
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		if(enemyCount < 0)
			enemyCount = 0;
		if(portalCount == 1 && !bossDead)
		{
			if(!spawnedBoss)SpawnBoss();
			spawnedBoss = true;
			lastPortal = true;
		}
	}

	public static void SpawnBaseEnemy(int numToSpawn, Transform trans)
	{
		Transform lastTrans = null;
		for(int i = 0; i <= numToSpawn; i++)
		{
			if(enemyCount < maxEnemies)
			{
				GameObject go = slimePool[slimeSpawnNum];
				EnemyBase enbase = go.GetComponent<EnemyBase>();
				go.SetActive(true);
				enbase._box.enabled = true;
				enbase._light.enabled = true;
				enbase.stolen = false;
				enbase.health = enbase.startingHealth;
				go.transform.position = trans.position;
				EventManager.WorldShiftInEvent += enbase.Shift;
				EventManager.WorldShiftOutEvent += enbase.UnShift;
				if(slimeSpawnNum >= slimePool.Length - 1)
					slimeSpawnNum = 0;
				else
					slimeSpawnNum++;
				enemyCount++;
			}
		}
	}

	public static void SpawnSecondEnemy(int numToSpawn, Transform trans)
	{
		Transform lastTrans = null;
		if(enemyCount < maxEnemies)
		{
			for(int i = 0; i <= numToSpawn; i++)
			{
				GameObject go = slimePoolLvl2[slimeSpawnLvl2Num];
				Slime2 enbase = go.GetComponent<Slime2>();
				go.SetActive(true);
				enbase._box.enabled = true;
				enbase._light.enabled = true;
				enbase.stolen = false;
				enbase.health = enbase.startingHealth;
				go.transform.position = trans.position;
				EventManager.WorldShiftInEvent += enbase.Shift;
				EventManager.WorldShiftOutEvent += enbase.UnShift;
				if(slimeSpawnLvl2Num >= slimePoolLvl2.Length - 1)
					slimeSpawnLvl2Num = 0;
				else
					slimeSpawnLvl2Num++;
				enemyCount++;
			}
		}
	}

	public void SpawnBoss()
	{
		var lastPortal = GameObject.Find ("PORTALANIM_0").transform.position;
		GameObject go = (GameObject)Instantiate(boss, lastPortal, Quaternion.identity);
	}
	public static void SpawnSlimeDeath(Transform trans)
	{
		GameObject go = slimeDeathPool[slimeNum];
		go.SetActive(true);
		go.transform.position = trans.position;
		if(slimeNum >= slimeDeathPool.Length - 1)
			slimeNum = 0;
		else
			slimeNum ++;
	}
}
